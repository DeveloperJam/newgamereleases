package controllers
import models.Game

import play.api._
import play.api.mvc._

import play.api.data._
import play.api.data.Forms._

object Application extends Controller {
  
def index = Action {
  Redirect(routes.Application.games)
}
def indexAdmin = Action {
  Ok(views.html.indexAdmin(Game.all(), gameForm))
}

val gameForm = Form(
   tuple
   (
  "image" -> nonEmptyText,
  "label" -> nonEmptyText,
  "descrip" -> nonEmptyText,
  "console" -> nonEmptyText,
  "releasedate" -> date,
  "rating" -> number

  )

)
  def games = Action {
  Ok(views.html.index(Game.all(), gameForm))
}
  
  def newGame = Action { implicit request =>
  gameForm.bindFromRequest.fold(
    errors => BadRequest(views.html.index(Game.all(), errors)),
    data  => {
      Game.create(data._2,data._1,data._3,data._4,data._5,data._6)
      Redirect(routes.Application.games)
    }
  )
}
  
 def deleteGame(id: Long) = Action {
  Game.delete(id)
  Redirect(routes.Application.games)
}
  
}