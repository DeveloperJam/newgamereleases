package models



import play.api.db._
import play.api.Play.current
import anorm._
import anorm.SqlParser._

case class Game(id: Long, label: String,image: String,descrip: String,console: String, releasedate:java.util.Date, rating:Integer)
object Game {

	val game = {
  get[Long]("id") ~ 
  get[String]("label") ~
  get[String]("image") ~
  get[String]("descrip") ~
  get[String]("console") ~
  get[java.util.Date]("releasedate") ~
  get[Int]("rating") map {
    case id~label~image~descrip~console~releasedate~rating => Game(id, label,image,descrip,console,releasedate,rating)
  }

}
  
  def all(): List[Game] = DB.withConnection { implicit c =>
  SQL("select * from game").as(game *)
}
  
  def create(label: String, image: String,descrip: String, console:String,releasedate:java.util.Date,rating:Integer) {
  DB.withConnection { implicit c =>
    SQL("insert into game (label,image,descrip,console,releasedate,rating) values ({label},{image},{descrip},{console},{releasedate},{rating})").on(
      'label -> label,
      'image -> image,
      'descrip -> descrip,
      'console -> console,
      'releasedate -> releasedate,
      'rating -> rating
    ).executeUpdate()
  }
}
  
  def delete(id: Long) {
  DB.withConnection { implicit c =>
    SQL("delete from game where id = {id}").on(
      'id -> id
    ).executeUpdate()
  }
}
  
}