# Tasks schema
 
# --- !Ups

CREATE SEQUENCE game_id_seq;
CREATE TABLE game (
    id integer NOT NULL DEFAULT nextval('game_id_seq'),
    label varchar(255),
    image varchar(255),
	descrip varchar(255),
	console varchar(255),
	releasedate date,
	rating integer

    );
 

